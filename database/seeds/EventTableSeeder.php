<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Event;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $location = \App\Location::create(['name'=>'Charlatan', 'address'=>'Erges in een straat']);
        $user = \App\User::create(['name'=>'random', 'email'=>'random@random.com', 'password'=>bcrypt('secret')]);
        $organizer = \App\Organizer::create(['user_id'=>$user->id, 'location_id'=>$location->id]);
        $faker = Faker\Factory::create();
        for($i=0; $i<6; $i++){
            for($j=0; $j<10; $j++){
                $this->createRandomEvent($faker, $organizer, $j);
            }
        }
    }

    private function createRandomEvent($faker, $organizer, $days){
        Event::create([
            'title' => $faker->words(rand(2,5), true),
            'description' => $faker->words(rand(10, 50), true ),
            'startDateTime' => \Carbon\Carbon::now()->addDays($days)->addHours(rand(0, 4))->addMinutes(rand(0, 59)),
            'organizer_id'=>$organizer->id,
            'location_id'=>$organizer->location->id,
            'confirmed' => rand(0,1)
        ]);
    }
}
