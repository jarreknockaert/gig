@extends('layouts.app')

@section('content')
    @include('forms.location', ['route' => ['locations.update', $location->id], 'method' => 'PUT'])
@endsection

