<?php

namespace App\Http\Controllers;

use Auth;
use Entrust;
use App\User;
use Illuminate\Http\Request;
use Hash;
use Validator;
use Redirect;
use App\Role;
use App\Http\Requests\SaveUserRequest;

class UserController extends Controller
{
    private $requestParams = array('name', 'email', 'activated');

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Entrust::can('read-users')) {
            $users = User::all();
            return view('users.index', compact('users'));
        } else {
            return view('exceptions.exception', ['statusCode' => 404]);
        }
    }

    public function show(User $user){
        return view('users.show', compact('user'));
    }

    public function edit(User $user)
    {
        if(Entrust::can('update-users')){
            return view('users.edit', compact('user'));
        }
        else {
            return view('exceptions.exception', ['statusCode' => 404]);
        }
    }
    //At the moment the admin can only changes a users role.
    public function update(Request $request, User $user)
    {
        if(Entrust::can('update-users')){
            $user->update(request($this->requestParams));
            UserController::updateRole($request, $user);
        }
        return redirect(route('users.index'));
    }




    public function updateRole(Request $request, User $user)
    {
        if(Entrust::can('update-users')){
            $user->detachRoles();
            $user->attachRole(Role::find($request->roleId));
            $user->save();
        }
    }
}