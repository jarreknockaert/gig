@extends('forms.general', ['title' => 'Locatie'])


@section('form')
    {{Form::open(array('route' => $route, 'method' => $method, 'class' => 'form-horizontal' ))}}
    @include('forms.locationInput', compact('locations'));
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            {{Form::submit('Submit', array('class' => 'btn btn-primary'))}}
        </div>
    </div>
    {{Form::close()}}

@endsection