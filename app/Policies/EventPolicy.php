<?php

namespace App\Policies;

use App\User;
use App\Event;

class EventPolicy
{
    public function update(User $user, Event $event){

        return $user->can('update-events') || $user->owns($event);
    }

    public function remove(User $user, Event $event){
        return $user->can('remove-events') || $user->owns($event);
    }
}
