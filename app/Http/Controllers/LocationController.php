<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;
use Entrust;

class LocationController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Entrust::can('read-locations')){
            $locations = Location::all();
            return view('locations.index', compact('locations'));
        }
        else {
            return view('exceptions.exception', ['statusCode' => 404]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     * If an ID is passed with the name 'locationId', no locations will be created
     *
     * @param  \Illuminate\Http\Request $request
     * @return The created locations
     */
    public function store(Request $request)
    {
        if($request->locationId){
            return Location::find($request->locationId);
        }
        else {
            return $this->configureLocation($request, new Location);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        return view('locations.edit', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        $this->configureLocation($request, $location);
        return redirect()->route('locations.index');
    }

    private function configureLocation(Request $request, Location $location){
        if($request->input('name' || $request->input('locationName'))){
            $location->name = $request->input('name') ?? $request->input('locationName');
        }
        if($request->input('address') || $request->input('locationAddress')){
            $location->address = $request->input('address') ?? $request->input('locationAddress');
        }
        $location->save();
        return $location;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $location->delete();
        return redirect()->route('locations.index');
    }
}
