@extends('forms.general', ['title' => 'Gebruiker'])

<?php
$role = $user->getRole();
$roles = \App\Role::pluck('display_name', 'id');
?>


@section('form')
    {{Form::model($user, array('route' => $route, 'method' => $method, 'class' => 'form-horizontal' ))}}
    <div class="form-group row">
        {{Form::label('name', 'Naam', array('class' => 'col-md-4 control-label'))}}
        <div class="col-md-6">
            {{Form::text('name', null, array('class' => 'form-control'))}}
        </div>
    </div>
    <div class="form-group row">
        {{Form::label('email', 'E-mail', array('class' => 'col-md-4 control-label'))}}
        <div class="col-md-6">
            {{Form::text('email', null, array('class' => 'form-control'))}}
        </div>
    </div>
    <div class="form-group row">
        {{Form::label('activated', 'Actief', array('class' => 'col-md-4 control-label'))}}
        <div class="col-md-6">
            {{Form::checkbox('activated', null, $user->activated, array('class' => 'form-control'))}}
        </div>
    </div>
    <div class="form-group row">
        {{Form::label('roleId', 'Rol', array('class' => 'col-md-4 control-label'))}}
        <div class="col-md-6">
            {{ Form::select('roleId', $roles, $role ? $role->id : '', ['id' => 'roleId', 'class' => 'form-control role-picker']) }}
        </div>
    </div>
    @include('partials.errors')
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            {{Form::submit('Submit', array('class' => 'btn btn-primary'))}}
        </div>
    </div>
    {{Form::close()}}
@endsection