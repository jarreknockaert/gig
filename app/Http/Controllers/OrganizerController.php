<?php

namespace App\Http\Controllers;

use Validator;
use Entrust;
use App\Location;
use App\Organizer;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\ActivationService;


class OrganizerController extends Controller
{

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    protected $activationService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ActivationService $activationService)
    {
        $this->middleware('auth');
        $this->activationService = $activationService;
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'locationName' => 'required|max:255',
            'locationAddress' => 'required|max:255'
        ]);
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->passes()) {
            $user = User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
            ]);
            $user->attachRole(Role::get('organizer'));
            $this->activationService->sendActivationMail($user);
            //Find the user and create an organizer.
            $location = Location::create([
                'name' => $request->input('locationName'),
                'address' => $request->input('locationAddress')
            ]);
            Organizer::create(['user_id' => $user->id, 'location_id' => $location->id]);
            return redirect('/login')->with('status', 'We sent you an activation code. Check your email.');
        }
        else {
            $this->throwValidationException(
                $request, $validator
            );
        }

    }

    public function showRegistrationForm(){
        return view('auth.organizerRegister');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Entrust::can('read-organizers')){
            $organizers = Organizer::all();
            return view('organizers.index', compact('organizers'));
        }
        else {
            return view('exceptions.exception', ['statusCode' => 404]);
        }
    }
}
