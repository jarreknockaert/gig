@extends('layouts.app')

@section('content')
    <table class="table profile">
        <tr><td>Naam</td>
            <td>{{$user->name}}</td>
            <td><a href="{{route('profile.name')}}">Bewerken</a></td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td>{{$user->email}}</td>
            <td><a href="{{route('profile.email')}}">Bewerken</a></td>
        </tr>
        @role('organizer')
            <tr>
                <td>Locatie naam</td>
                <td>{{$location->name ?? '' }}</td>
                <td><a href="{{route('profile.locationName')}}">Bewerken</a></td>
            </tr>
            <tr>
                <td>Locatie adres</td>
                <td>{{$location->address ?? ''}}</td>
                <td><a href="{{route('profile.locationAddress')}}">Bewerken</a></td>
            </tr>
        @endrole
        <tr>
            <td></td>
            <td></td>
            <td><a href="{{route('profile.password')}}">Wachtwoord bewerken</a></td>
        </tr>
    </table>
@endsection