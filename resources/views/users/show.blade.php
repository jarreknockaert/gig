@extends('layouts.app')

@section('content')
    <table class="table profile">
        <tr><td>Naam</td>
            <td>{{$user->name}}</td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td>{{$user->email}}</td>
        </tr>
        <tr>
            <td>Actief</td>
            <td>{{$user->active ? 'Ja' : 'Nee'}}</td>
        </tr>
        <tr>
            <td>Rol</td>
            <td class="input-col">@include('partials.roleSelect', ['user' => $user])</td>
        </tr>
    </table>
@endsection

