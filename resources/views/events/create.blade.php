@extends('layouts.app')

@section('content')
    @include('forms.event', ['route' => 'events.store', 'method' => 'POST'])
@endsection

