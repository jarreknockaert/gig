<?php

namespace App\Http\Controllers;

use Gate;
use Entrust;
use Auth;
use Redirect;
use App\Event;
use Illuminate\Http\Request;
use App\Http\Requests\SaveEventRequest;

class EventController extends Controller
{
    private $requestParams = array('title', 'description', 'startDateTime');

    public function __construct(){
        $this->middleware('auth', ['except' => ['showWeek', 'getEventsWithinDays', 'show']]);
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::inFuture()->sorted()->get();
        return view('events.index', compact('events'));
    }

    /**
     * Display a listing of the events of the authenticated organizer.
     */
    public function organizerIndex(){
        if(Entrust::hasRole('organizer')){
            $events = Event::where('organizer_id', Auth::user()->getOrganizer()->id)->get();
            return view('events.index', compact('events'));
        }
        else {
            return view('exceptions.exception', ['statusCode' => 404]);
        }
    }

    public function showWeek(){
        return view('events.currentWeek');
    }

    /**
     * @param $day Amount of days relative to today.
     * @return mixed Events on date of today + $days
     */
    public function getEventsWithinDays($days){
        $eventsDay = Event::withinDays($days)->confirmed()->sorted()->get();
        foreach($eventsDay as $event){
            if($event->location){
                $event->locationName = $event->location->name;
            }
        }
        $data['events'] = $eventsDay;
        return $data;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Entrust::can('create-events')){
            return view('events.create');
        }
        else {
            return view('exceptions.exception', ['statusCode' => 404]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SaveEventRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveEventRequest $request)
    {
        $event = Event::create(request($this->requestParams));
        if(Entrust::can('create-events')){
            $event->confirmed = true;
        }
        $location = (new LocationController())->store($request);
        $event->location_id = $location->id;
        $event->save();
        return redirect()->route('events.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        return view('events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        if(Gate::allows('update', $event)){
            return view('events.edit', compact('event'));
        }
        else {
            return view('exceptions.exception', ['statusCode' => 404]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SaveEventRequest  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(SaveEventRequest $request, Event $event)
    {
        $event->update(request($this->requestParams));
        (new LocationController())->update($request, $event->location);
        return redirect()->route('events.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function setConfirmed(Request $request, Event $event)
    {
        $event->confirmed = $request->input('confirmed');
        $event->save();
        return $event;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        if(Gate::allows('delete', $event)){
            $event->delete();
            return redirect()->route('events.index');
        }
        else {
            return view('exceptions.exception', ['statusCode' => 404]);
        }
    }



}
