@extends('layouts.app')

@section('content')
    @include('forms.event', ['route' => ['events.update', $event->id], 'method' => 'PUT'])
@endsection

