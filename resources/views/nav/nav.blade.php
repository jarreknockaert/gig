

<nav class="navbar navbar-default ">
    <div class="container">
        <a class="navbar-brand" href="{{route('home')}}">{{env('APP_NAME')}}</a>
        <ul class="nav navbar-nav navbar-right collapse navbar-collapse">
        @if (Auth::guest())
            <!--@include('nav.link', ['name' => 'login', 'title' => 'Login'])-->
            <!--@include('nav.link', ['name' => 'register', 'title' => 'Registreer'])-->
                @include('nav.link', ['name' => 'about', 'title' => 'Over ons'])
                @include('nav.link', ['name' => 'subscribe', 'title' => 'Inschrijven op onze nieuwsbrief'])
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Meehelpen</a>
                    <ul class="dropdown-menu" role="menu">
                        @include('nav.link', ['name' => 'donate', 'title' => 'Doneer'])
                        @include('nav.link', ['name' => 'volunteer', 'title' => 'Vrijwilliger worden'])
                    </ul>
                </li>
            @else
                @if (Entrust::hasRole('editor') || Entrust::hasRole('admin'))
                    @include('nav.link', ['name' => 'events.index', 'title' => 'Alle evenementen'])
                @endif
                @if (Entrust::hasRole('organizer'))
                    @include('nav.link', ['name' => 'organizer.events', 'title' => 'Mijn evenementen'])
                @endif
                @include('nav.link', ['name' => 'events.create', 'title' => 'Nieuw evenement'])
                @if (Entrust::hasRole('admin'))
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Admin</a>
                        <ul class="dropdown-menu" role="menu">
                            @include('nav.link', ['name' => 'organizers.index', 'title' => 'Organisatoren'])
                            @include('nav.link', ['name' => 'users.index', 'title' => 'Gebruikers'])
                            @include('nav.link', ['name' => 'locations.index', 'title' => 'Locaties'])
                        </ul>
                    </li>
                @endif
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }}</span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        @include('nav.link', ['name' => 'profile', 'title' => 'Profiel'])
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>

    </div>
</nav>