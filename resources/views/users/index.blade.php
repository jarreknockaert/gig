@extends('layouts.app')

@section('content')
    <table class="table">
        <tr>
            <th>Naam</th>
            <th>E-mail</th>
            <th>Rol</th>
            <th>Actief</th>
        </tr>
        @foreach($users as $user)
            <tr>
                <td class="eh"><a href="{{route('users.show', ['user' => $user->id])}}">{{$user->name}}</a></td>
                <td>{{$user->email}}</td>
                <td>{{$user->getRole() ? $user->getRole()->display_name : ''}}</td>
                <td>{{$user->activated ? ' Ja' : 'Nee'}}</td>
                <td><a href="{{route('users.edit', ['id' => $user->id])}}">Wijzig</a></td>
                <td><button type="button" class="btn btn-link" onclick="document.getElementById('remove-user').submit()">Verwijder</button>
                    {{ Form::open(array('route' => array('users.destroy', $user), 'method' => 'DELETE', 'id' => 'remove-user')) }}</td>
            </tr>
        @endforeach
    </table>
@endsection

