@extends('layouts.app')

@section('content')
    @include('forms.user', ['route' => ['users.update', $user->id], 'method' => 'PUT'])
@endsection

