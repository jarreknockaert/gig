@extends('layouts.app')


@section('content')
    <div class="card">
        <div class="card-header">
            {{$title}} wijzigen
        </div>
        <div class="card-block">
            @include('partials.errors')
            {{Form::open(array('action' => $action, 'method' => 'PUT'))}}
            <div class="form-group">
                {{Form::label($property, $title, array('class' => 'form-control-label'))}}
                {{Form::text($property, $object[$propertyName ?? $property],array('class'=>'form-control form-border-bottom'))}}
            </div>
        </div>
        <div class="modal-footer">
            <a href="{{route('profile')}}" class="btn btn-default">Annuleren</a>
            {{Form::submit('Gereed', array('class' => 'btn btn-primary'))}}
        </div>
        {{Form::close()}}
    </div>
@endsection