<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('donate', function(){ return view('guest.donate');})->name('donate');
Route::get('volunteer', function (){ return view('guest.volunteer');})->name('volunteer');
Route::get('subscribe', function (){ return view('guest.subscribe');})->name('subscribe');
Route::get('about', function (){ return view('guest.about');})->name('about');

Route::get('user/activation/{token}', 'Auth\RegisterController@askAdminConfirmation')->name('user.activate');
Route::get('admin/activation/{token}', 'Auth\RegisterController@activateUser')->name('admin.activate');

Route::resource('events', 'EventController');
Route::put('events/{event}/confirm', 'EventController@setConfirmed');
Route::resource('locations', 'LocationController');

Route::get('', 'EventController@showWeek')->name('home'); //Change this to EventController@showWeek

Route::get('events/within_days/{days}', 'EventController@getEventsWithinDays');
Route::get('organizer/events', 'EventController@organizerIndex')->name('organizer.events');
Route::get('organizer/register', 'OrganizerController@showRegistrationForm');
Route::post('organizer/register', 'OrganizerController@register')->name('organizer.register');

Route::get('users', 'UserController@index')->name('users.index');
Route::get('users/{user}', 'UserController@show')->name('users.show');
Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit');
Route::put('users/{user}', 'UserController@update')->name('users.update');
Route::put('users/{user}/role', 'UserController@updateRole')->name('users.updateRole');
Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy');
Route::get('organizers', 'OrganizerController@index')->name('organizers.index');

Route::get('profile', 'ProfileController@show')->name('profile');
Route::get('profile/password', 'ProfileController@editPassword')->name('profile.password');
Route::get('profile/name', 'ProfileController@editName')->name('profile.name');
Route::get('profile/email', 'ProfileController@editEmail')->name('profile.email');
Route::get('profile/location/name', 'ProfileController@editLocationName')->name('profile.locationName');
Route::get('profile/location/address', 'ProfileController@editLocationAddress')->name('profile.locationAddress');
Route::put('profile/password', 'ProfileController@updatePassword');
Route::put('profile/name', 'ProfileController@updateName');
Route::put('profile/email', 'ProfileController@updateEmail');
Route::put('profile/location/name', 'ProfileController@updateLocationName');
Route::put('profile/location/address', 'ProfileController@updateLocationAddress');





