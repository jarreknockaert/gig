<?php
namespace App;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Connection;

class ActivationRepository
{

    protected $db;

    protected $table = 'user_activations';

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function getUser($activation){
        return User::find($activation->user_id);
    }

    protected function getToken()
    {
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }

    /**
     * @param $user
     * @return array 0: user_token, 1: admin_token
     */
    public function createActivation($user)
    {

        $activation = $this->getActivation($user);

        if (!$activation) {
            return $this->createToken($user);
        }
        return $this->regenerateTokens($user);

    }

    /**
     * @param $user
     * @return array 0: user_token, 1: admin_token
     */
    private function regenerateTokens($user)
    {
        $userToken = $this->getToken();
        $adminToken = $this->getToken();
        $this->db->table($this->table)->where('user_id', $user->id)->update([
            'user_token' => $userToken,
            'admin_token' => $adminToken,
            'created_at' => new Carbon()
        ]);
        return [$userToken, $adminToken];
    }

    private function createToken($user)
    {
        $userToken = $this->getToken();
        $adminToken = $this->getToken();
        $this->db->table($this->table)->insert([
            'user_id' => $user->id,
            'user_token' => $userToken,
            'admin_token' => $adminToken,
            'created_at' => new Carbon()
        ]);
        return [$userToken, $adminToken];
    }

    public function getActivation($user)
    {
        return $this->db->table($this->table)->where('user_id', $user->id)->first();
    }


    private function getActivationByToken($column, $token)
    {
        return $this->db->table($this->table)->where($column, $token)->first();
    }

    public function getActivationByAdminToken($token){
        return $this->getActivationByToken('admin_token', $token);
    }

    public function getActivationByUserToken($token){
        return $this->getActivationByToken('user_token', $token);
    }

    public function deleteActivation($token)
    {
        $this->db->table($this->table)->where('user_token', $token)->delete();
    }

}