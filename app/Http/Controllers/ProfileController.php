<?php

namespace App\Http\Controllers;

use Auth;
use Entrust;
use App\User;
use Illuminate\Http\Request;
use Hash;
use Validator;
use Redirect;

class ProfileController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function show()
    {
        $user = Auth::user();
        $organizer = $user->getOrganizer();
        if($organizer){
            $location = $organizer->location;
            return view('users.profile', compact('user', 'location'));
        }
        else {
            return view('users.profile', compact('user'));
        }
    }


    public function editName(){
        $user = Auth::user();
        return view('users.editProfile', ['title' => 'Naam', 'property' => 'name', 'object' => $user, 'action' => 'ProfileController@updateName']);
    }

    public function updateName(Request $request){
        $this->validate($request, ['name|max:255' => 'required']);
        return $this->updateProperty($request, Auth::user(), 'name');
    }

    public function editEmail(){
        $user = Auth::user();
        return view('users.editProfile', ['title' => 'E-mail', 'property' => 'email', 'object' => $user, 'action' => 'ProfileController@updateEmail']);
    }

    public function updateEmail(Request $request){
        $this->validate($request, ['email' => 'required|email|max:255|unique:users']);
        return $this->updateProperty($request, Auth::user(), 'email');
    }

    public function editLocationName(){
        if($organizer = Auth::user()->getOrganizer()){
            return view('users.editProfile',
                ['title' => 'Naam locatie', 'property' => 'locationName',
                    'propertyName' => 'name', 'object' => $organizer->location, 'action' => 'ProfileController@updateLocationName']);
        }
        else {
            return view('exceptions.exception', ['statusCode' => 404]);
        }
    }

    public function updateLocationName(Request $request){
        $this->validate($request, ['locationName' => 'required|max:255']);
        return $this->updateProperty($request, Auth::user()->getOrganizer()->location, 'locationName', 'name');
    }

    public function editLocationAddress(){
        if($organizer = Auth::user()->getOrganizer()){
            return view('users.editProfile',
                ['title' => 'Adres locatie', 'property' => 'locationAddress',
                    'propertyName' => 'address', 'object' => $organizer->location, 'action' => 'ProfileController@updateLocationAddress']);
        }
        else {
            return view('exceptions.exception', ['statusCode' => 404]);
        }
    }

    public function updateLocationAddress(Request $request){
        $this->validate($request, ['locationAddress' => 'required|max:255']);
        return $this->updateProperty($request, Auth::user()->getOrganizer()->location, 'locationAddress', 'address');
    }

    public function editPassword(){
        return view('users.editPassword', ['action' => 'ProfileController@updatePassword']);
    }

    public function updatePassword(Request $request)
    {
        $user = Auth::user();
        $validator = $this->admin_credential_rules($request->all());
        if(!$validator->fails())
        {
            $currentPassword = $user->password;
            if(Hash::check($request['currentPassword'], $currentPassword))
            {
                $user->password = Hash::make($request['newPassword']);;
                $user->save();
                return $this->show();
            }
            else
            {
                $validator->errors()->add('currentPassword', 'Please enter correct password');
            }
        }
        return redirect(route('profile.password'))->withErrors($validator);
    }

    private function admin_credential_rules(array $data)
    {
        $messages = [
            'currentPassword.required' => 'Please enter current password',
            'newPassword.required' => 'Please enter a new password',
        ];

        $validator = Validator::make($data, [
            'currentPassword' => 'required',
            'newPassword' => 'required|min:6|same:newPassword',
            'confirmedPassword' => 'required|min:6|same:newPassword',
        ], $messages);
        return $validator;
    }

    private function updateProperty(Request $request, $object, $property, $propertyName = null){
        $object[$propertyName ?? $property] = $request[$property];
        $object->save();
        return $this->show();
    }
}
