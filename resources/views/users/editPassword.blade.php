@extends('layouts.app')


@section('content')
    <div class="card">
        <div class="card-header">
            Wachtwoord wijzigen
        </div>
        <div class="card-block">
            @include('partials.errors')
            {{Form::open(array('action' => $action, 'method' => 'PUT'))}}
            <div class="form-group">
                {{Form::label('currentPassword', 'Huidig', array('class' => 'form-control-label'))}}
                {{Form::password('currentPassword', array('class'=>'form-control form-border-bottom'))}}
            </div>
            <div class="form-group">
                {{Form::label('newPassword', 'Nieuw', array('class' => 'form-control-label'))}}
                {{Form::password('newPassword', array('class'=>'form-control form-border-bottom'))}}
            </div>
            <div class="form-group">
                {{Form::label('confirmedPassword', 'Wachtwoord opnieuw invoeren', array('class' => 'form-control-label'))}}
                {{Form::password('confirmedPassword', array('class'=>'form-control form-border-bottom'))}}
            </div>
        </div>
        <div class="modal-footer">
            <a href="{{route('profile')}}" class="btn btn-default">Annuleren</a>
            {{Form::submit('Gereed', array('class' => 'btn btn-primary'))}}
        </div>
        {{Form::close()}}
    </div>
@endsection