<?php

namespace App;

class Organizer extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function location(){
        return $this->belongsTo(Location::class);
    }

    public function owns(Event $event){
        if($event->organizer){
            return $this->id == $event->organizer->id;
        }
        else {
            return false;
        }
    }
}
