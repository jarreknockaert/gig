    @extends('layouts.app')

    @section('content')
        <div class="page-header">
            <h1>Vrijwilliger worden</h1>
        </div>
        <h2>Wa moek doen?</h2>

        <p>
            Als vrijwilliger voor Gratis in Gent kiest ge zelf uw engagement. Of ge nu één keer per jaar de nieuwsbrief ineen wilt steken, of ge wilt dat iedere week doen, dat maakt ons allemaal niet uit.
        </p>

        <h2>En wa krijggek?</h2>

        <p>
            Qua centen, niets. Koekskes, dat wel.
            Wij doen dat hier allemaal gratis. Ge leert vooral het Gentse cultuurlandschap vré goed kennen. En af en toe gaan we samen pintjes gaan drinken. Ge kunt ook op uw seevee schrijven dat ge wekelijks nieuwsbrieven naar 20.000 man hebt verstuurd.
        </p>

        <h2>En hoe weet ik dat gulder geen vieze gasten zijt?</h2>

        <p>Omdat iedereen ons waas vind waar. Wij zijn mee drie en tope zijn we 55 jaar.</p>

        <h2>Alé, ondanks ulder prietpraat heb ik wel goeste om mee te doen.</h2>

        <p>Stuur ons tans een mailtje op info@gratisingent.be, dan gaan we eens een pintje of gemberlimonade drinken en leggen we het u allemaal eens tegoei uit.</p>
    @endsection