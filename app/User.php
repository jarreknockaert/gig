<?php

namespace App;

use App\Notifications\ResetPasswordEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Returns the admin account sending emails
     */
    public static function getAdminMailAccount(){
        return User::where('email', env('MAIL_ADMIN'))->get()->first();
    }

    /**
     * This function returns the organizer which owns this account if the account belongs to an organizer.
     */
    public function getOrganizer(){
        return Organizer::where('user_id', $this->id)->get()->first();
    }


    public function owns(Event $event){
        if($this->getOrganizer() != null){
            return $this->getOrganizer()->owns($event);
        }
        return false;
    }

    public function roles(){
        return $this->belongsToMany(Role::class);
    }

    public static function get(String $email){
        return User::where('email', $email)->get()->first();
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordEmail($token));
    }

    public function getRole(){
        $role_user = DB::table('role_user')->where('user_id', $this->id)->get()->first();
        if($role_user){
            return DB::table('roles')->find($role_user->role_id);
        }
    }
}
