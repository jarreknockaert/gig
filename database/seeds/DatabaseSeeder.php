<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Role;
use App\User;
use App\Event;
use App\Organizer;
use App\Location;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->delete();
        DB::table('organizers')->delete();
        DB::table('locations')->delete();
        DB::table('roles')->delete();
        DB::table('users')->delete();

        $this->call(EventTableSeeder::class);
        $this->call(PermissionTableSeeder::class);

        $adminAccount = User::create(['name' => 'Jarre', 'email' => 'admin@gig.be', 'password' => bcrypt('password'), 'activated' => true]);
        $editorAccount = User::create(['name' => 'Lander', 'email' => 'editor@gig.be', 'password' => bcrypt('password'), 'activated' => true]);
        $organizerAccount = User::create(['name' => 'Giele', 'email' => 'organizer@gig.be', 'password' => bcrypt('password'), 'activated' => true]);


        $adminAccount->attachRole(Role::get('admin'));
        $editorAccount->attachRole(Role::get('editor'));
        $organizerAccount->attachRole(Role::get('organizer'));

        $location = Location::create(['name' => 'Jarre\'s kot', 'address' => 'De beste straat in Gent']);
        $organizer = Organizer::create(['user_id' => $organizerAccount->id, 'location_id' => $location->id]);



        //Add events to the organizer
        Event::create(['title' => 'Event 1', 'description' => 'Beschrijving 1', 'startDateTime' => Carbon\Carbon::now(), 'location_id' => $location->id, 'organizer_id' => $organizer->id]);
        Event::create(['title' => 'Event 2', 'description' => 'Beschrijving 2', 'startDateTime' => Carbon\Carbon::now(), 'location_id' => $location->id, 'organizer_id' => $organizer->id]);


    }
}
