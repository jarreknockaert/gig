@extends('layouts.app')

@section('content')
    <h1>{{$event->title}}</h1>
    <p>{{$event->description}}</p>
    @can('update', $event)
        <a href="{{route('events.edit', ['id' => $event->id])}}" class="btn btn-primary">Edit</a>
    @endcan
    @can('remove', $event)
        <button type="button" class="btn btn-danger" onclick="document.getElementById('remove-event').submit()">Remove</button>
        {{ Form::open(array('route' => array('events.destroy', $event), 'method' => 'DELETE', 'id' => 'remove-event')) }}
    @endcan
@endsection

