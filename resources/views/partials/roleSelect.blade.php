<?php
$role = $user->getRole();
$roles = \App\Role::pluck('display_name', 'id');
?>


{{ Form::select('roleId', $roles, $role ? $role->id : '', ['id' => 'roleId', 'class' => 'form-control role-picker']) }}

<div class="modal fade" id="roleSubmit" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Bevestiging</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Weet u zeker dat u {{$user->name}} de rol <span id="role"></span> wilt toekennen?</p>
            </div>
            <div class="modal-footer">
                <button id="submit" type="button" class="btn btn-primary role-submit">Save changes</button>
                <button id="cancel" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    <script>
        $(document).ready(function() {
            var selector = document.getElementById('roleId')
            var oldIndex = selector.selectedIndex
            selector.addEventListener('change', function () {
                document.getElementById('role').innerHTML = selector.options[selector.selectedIndex].textContent.toLocaleLowerCase();
                $('#roleSubmit').modal('show');
            })
            $('#submit').click(function () {
                var userId = "<?php echo $user->id; ?>";
                var roleId = selector.options[selector.selectedIndex].value
                $.ajax({
                    type: "PUT",
                    url: "/users/" + userId + "/role",
                    data: {
                        'roleId': roleId
                    },
                    success: function (data) {
                        $('#roleSubmit').modal('hide');
                        oldIndex = selector.selectedIndex;
                    },
                    error: function (data) {
                        selector.selectedIndex = oldIndex;
                    }
                })
            })
            $('#cancel').click(function(){
                selector.selectedIndex = oldIndex;
            })
        });
    </script>
@endsection