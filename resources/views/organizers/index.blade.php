@extends('layouts.app')

@section('content')
    <table class="table">
        <tr>
            <th>Naam</th>
            <th>E-mail</th>
            <th>Naam locatie</th>
            <th>Adres locatie</th>
        </tr>
        @foreach($organizers as $organizer)
            <tr>
                <td>{{$organizer->user->name}}</td>
                <td>{{$organizer->user->email}}</td>
                @if($organizer->location)
                    <td>{{$organizer->location->name}}</td>
                    <td>{{$organizer->location->address}}</td>
                @else
                    <td>Geen</td><td>Geen</td>
                @endif
            </tr>
        @endforeach
    </table>

@endsection