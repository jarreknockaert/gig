@permission('read-locations')
    @if(!$location)
        <?php
        $locations = \App\Location::pluck('name', 'id');
        ?>

        <div class="form-group row">
            {{Form::label('locations', 'Locatie', array('class' => 'col-md-4 control-label'))}}
            <div class="col-md-6">
                {{ Form::select('locationId', $locations, null, ['id' => 'locationId', 'class' => 'form-control']) }}
            </div>
        </div>

        <script type="text/javascript">
            $('select').select2();
        </script>
    @endif
@endpermission

<div class="form-group row ">
    {{Form::label('locationName', 'Naam locatie', array('class' => 'col-md-4 control-label'))}}
    <div class="col-md-6">
        {{Form::input('input','locationName', $location->name ?? null, array('class' => 'form-control select'))}}
    </div>
</div>
<div class="form-group row ">
    {{Form::label('address', 'Adres locatie', array('class' => 'col-md-4 control-label'))}}
    <div class="col-md-6">
        {{Form::input('input','address', $location->address ?? null, array('class' => 'form-control'))}}
    </div>
</div>

