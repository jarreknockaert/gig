@extends('layouts.app')

@section('content')

    <table id="all-events" class="table">
        <tr>
            <th>Titel</th>
            <th>Beschrijving</th>
            <th>Locatie</th>
            <th>Tijdstip</th>
            @permission('update-events')
                <th>Bevestigd</th>
            @endpermission
        </tr>
        @foreach($events as $event)
            <tr>
                <td><a href="{{route('events.show', compact('event'))}}">{{$event->title}}</a></td>
                <td class="desc">{{$event->description}}</td>
                <td>{{$event->location->name}}</td>
                <td>{{\Carbon\Carbon::parse($event->startDateTime)}}</td>
                @permission('update-events')
                    <td>
                        @if(!$event->confirmed)
                            <button type="button" id="btn{{$event->id}}" class="btn btn-link" onclick="confirm({{$event->id}})">Bevestigen</button>
                        @else
                            <button type="button" id="btn{{$event->id}}" class="btn btn-link" onclick="cancel({{$event->id}})">Annuleren</button>
                        @endif
                    </td>
                @endpermission
            </tr>
        @endforeach
    </table>
@endsection



@section('scripts')
    <script>
        function truncateText(description, maxLength) {
            var truncated = description.innerText;
            if (truncated.length > maxLength) {
                truncated = truncated.substr(0,maxLength) + '...';
            }
            description.innerText = truncated;
        }
        function confirm(id){
            updateEvent(id, 1)
        }
        function cancel(id){
            updateEvent(id, 0)
        }
        function updateEvent(id, status){
            $.ajax({
                type: "PUT",
                url: "/events/" + id + '/confirm',
                data: {
                    'confirmed': status
                },
                success: function (data) {
                    //Switch function of button
                    var btn = $('#btn' + id);
                    btn.attr("onclick", data['confirmed'] === "1" ? 'cancel(' + id + ')' : 'confirm(' + id + ')');
                    btn.text(data['confirmed'] === "1" ?  'Annuleren' : 'Bevestigen');
                }
            })
        }
        $(document).ready(function() {
            let descriptions = document.getElementsByClassName('desc')
            for(var i=0; i<descriptions.length; i++){
                truncateText(descriptions[i], 50);
            }
        })
    </script>
@endsection