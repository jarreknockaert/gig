<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Role;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::create(['name' => 'admin', 'display_name' => 'Administrator']);
        $editorRole = Role::create(['name' => 'editor', 'display_name' => 'Redactielid']); //Mensen van de redactie
        $organizerRole = Role::create(['name' => 'organizer', 'display_name' => 'Organisator']);

        $createEvents = Permission::create(['name' => 'create-events']);
        $deleteEvents = Permission::create(['name' => 'delete-events']);
        $updateEvents = Permission::create(['name' => 'update-events']);
        $confirmEvents = Permission::create(['name' => 'confirm-events']);

        $readLocations = Permission::create(['name' => 'read-locations']);
        $updateLocations = Permission::create(['name' => 'update-locations']);
        $removeLocations = Permission::create(['name' => 'remove-locations']);
        //Other permissions are exclusive for the admin. These can be checked by checking if the user has the admin role.

        $readUsers = Permission::create(['name' => 'read-users']);
        $updateUsers = Permission::create(['name' => 'update-users']);
        $readOrganizers = Permission::create(['name' => 'read-organizers']);

        $adminRole->attachPermissions(array(
            $createEvents, $deleteEvents, $updateEvents, $confirmEvents,
            $readLocations, $updateLocations, $removeLocations,
            $readUsers, $readOrganizers, $updateUsers
        ));

        $editorRole->attachPermissions(array(
            $createEvents, $updateEvents, $deleteEvents, $readLocations, $updateLocations
        ));

        $organizerRole->attachPermissions(array(
            $createEvents
        ));

        //The organizer can only configure his own locations and events. This is decided with policies.

    }
}
