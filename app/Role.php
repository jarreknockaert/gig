<?php
/**
 * Created by PhpStorm.
 * User: Jarre
 * Date: 24/04/2017
 * Time: 22:03
 */

namespace App;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    /**
     * Get the role object with the given name.
     * @param String $name Name of the role
     * @return Role
     */
    public static function get(String $name){
        return Role::where('name', $name)->get()->first();
    }

    public function permissions(){
        return $this->belongsToMany(Permission::class);
    }
}