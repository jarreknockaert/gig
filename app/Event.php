<?php

namespace App;

use \Carbon\Carbon;

class Event extends Model
{
    public function organizer(){
        return $this->belongsTo(Organizer::class);
    }

    public function location(){
        return $this->belongsTo(Location::class);
    }

    public function scopeInFuture($query){
        return $query->whereDate('startDateTime', '>', Carbon::today('Europe/Brussels'));
    }

    public function scopeSorted($query){
        return $query->orderBy('startDateTime');
    }

    public function scopeConfirmed($query){
        return $query->where('confirmed', true);
    }

    public function scopeInconfirmed($query){
        return $query->where('confirmed', false);
    }

    public function scopeWithinDays($query, $days){
        return $query->whereDate('startDateTime', Carbon::today('Europe/Brussels')->addDays($days));
    }
}
