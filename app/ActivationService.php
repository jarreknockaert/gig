<?php

namespace App;


use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use App\Notifications\ConfirmEmailEditor;
use App\Notifications\ConfirmEmailAdmin;
use App\User;

class ActivationService
{

    protected $mailer;

    protected $activationRepo;

    protected $resendAfter = 24;

    public function __construct(Mailer $mailer, ActivationRepository $activationRepo)
    {
        $this->mailer = $mailer;
        $this->activationRepo = $activationRepo;
    }

    public function sendActivationMail($user)
    {
        if ($user->activated || !$this->shouldSend($user)) {
            return;
        }

        $token = $this->activationRepo->createActivation($user)[0];

        $link = route('user.activate', $token);
        $message = sprintf('Activate account <a href="%s">%s</a>', $link, $link);

        $user->notify(new ConfirmEmailEditor($token));
    }

    public function sendActivationMailAdmin($userToken){
        $activation = $this->activationRepo->getActivationByUserToken($userToken);
        $user = $this->activationRepo->getUser($activation);
        if ($user->activated) {
            return ;
        }
        $adminToken = $this->activationRepo->createActivation($user)[1];
        $link = route('admin.activate', $adminToken);
        $message = sprintf('Activate account <a href="%s">%s</a>', $link, $link);
        User::getAdminMailAccount()->notify(new ConfirmEmailAdmin($adminToken));
        return 'Gebruiker aangemaakt';
    }

    public function userTokenExists($token){
        return $this->activationRepo->getActivationByUserToken($token) !=  null;
    }

    public function activateUser($token)
    {
        $activation = $this->activationRepo->getActivationByAdminToken($token);

        if ($activation === null) {
            return null;
        }

        $user = User::find($activation->user_id);

        $user->activated = true;

        $user->save();

        $this->activationRepo->deleteActivation($token);

        return $user;

    }

    private function shouldSend($user)
    {
        $activation = $this->activationRepo->getActivation($user);
        return $activation === null || strtotime($activation->created_at) + 60 * 60 * $this->resendAfter < time();
    }

}