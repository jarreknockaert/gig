@extends('layouts.app')


<?php
        switch($statusCode){
            case 403:
                $statusMessage = 'Niet geauthoriseerd';
                break;
            default:
                $statusMessage = 'Niet gevonden';
        }
?>

@section('content')
    <h1 class="text-center" id="error-message">{{$statusCode}}: {{$statusMessage}}</h1>
@endsection


