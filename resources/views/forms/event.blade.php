@extends('forms.general', ['title' => 'Event'])


<?php
//Prepare the form.
$datetime = \Carbon\Carbon::now('Europe/Brussels');
if(Request::route('event')){ $datetime = new DateTime($event->startDateTime); }
$datetimeString = $datetime->format('Y-m-d\TH:i');
$organizer = Auth::user()->getOrganizer();
if($organizer){
    $location = $organizer->location;
}
else {
    $location = null;
}
?>

<!--
TODO: Verschillende pagina's: admin.gig.be, gig.be, organizer.gig.be?
-->

@section('form')
    @if(Request::route('event'))
        {{Form::model($event, array('route' => $route, 'method' => $method, 'class' => 'form-horizontal' ))}}
    @else
        {{Form::open(array('route' => $route, 'method' => $method, 'class' => 'form-horizontal' ))}}
    @endif
    <div class="form-group row">
        {{Form::label('title', 'Titel', array('class' => 'col-md-4 control-label'))}}
        <div class="col-md-6">
            {{Form::text('title', null, array('class' => 'form-control'))}}
        </div>
    </div>
    <div class="form-group row">
        {{Form::label('description', 'Omschrijving', array('class' => 'col-md-4 control-label'))}}
        <div class="col-md-6">
            {{Form::textarea('description', null, array('class' => 'form-control'))}}
        </div>
    </div>
    <div class="form-group row ">
        {{Form::label('startDateTime', 'Start tijdstip', array('class' => 'col-md-4 control-label'))}}
        <div class="col-md-6">
            {{Form::input('datetime-local','startDateTime', $datetimeString, array('class' => 'form-control'))}}
        </div>
    </div>
    @include('forms.locationInput', ['locations' => $event->location ?? $location])
    @include('partials.errors')
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            {{Form::submit('Submit', array('class' => 'btn btn-primary'))}}
        </div>
    </div>
    {{Form::close()}}
@endsection