@extends('layouts.app')

<?php
$days = ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag', 'Zondag'];
$today = \Carbon\Carbon::now('Europe/Brussels')->dayOfWeek;
?>

@section('content')
    <div class="content">
        <nav class="navbar nav-pills">
            <ul class="navbar-nav navbar-week">
                <li class="nav-item"><button type="button" id="btn0" class="btn btn-secondary" onclick="showDay(0)">Vandaag</button></li>
                <li class="nav-item"><button type="button" id="btn1" class="btn btn-secondary" onclick="showDay(1)">Morgen</button></li>
                @for($day=$today+2, $i=2; $i<7; $day++, $i++)
                    <li class="nav-item"><button type="button" id="btn{{$i}}" class="btn btn-secondary" onclick="showDay({{$i}})">{{$days[($day-1)%7]}}</button></li>
                @endfor
            </ul>
        </nav>
        <div id="events">
        </div>
        <div id="loading"><i class="fa fa-spinner fa-spin"></i></div>
    </div>


@endsection

@section('scripts')
    <script>
        var activeButton = $('#btn0');
        let showDay = function(days){
            $('#loading').show();
            var body = document.getElementById('events');
            body.innerHTML = ''
            activeButton.removeClass('active');
            activeButton = $('#btn'+days)
            activeButton.addClass('active');
            $.ajax({
                type: "GET",
                url: "events/within_days/" + days,
                success: function (data) {
                    let events = data['events']
                    if(events.length > 0){
                        for(let i=0; i<events.length; i++){
                            body.innerHTML += showEvent(events[i])
                        }
                    }
                    else {
                        body.innerHTML = '<p class="none">Geen evenementen gevonden.<p>'
                    }

                },
                error: function (data) {
                    body.innerHTML = '<p class="none">Probleem met het laden van de evenementen. Probeer later opnieuw.</p>'
                }
            })
        };

        let showEvent = function(event){
            var dateTime = event['startDateTime'];
            var hours = new Date(dateTime).getHours();
            var minutes = new Date(dateTime).getMinutes();
            minutes = minutes < 10 ? '0' + minutes : minutes
            var locationHtml = '';
            if(event['locationName']){
                locationHtml = `<small><strong>${event['locationName']}</strong></small>`
            }
            return `<a href="/events/${event['id']}" class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex justify-content-between">
                <h2 class="event-title">${event['title']}</h2>
                <small>${hours}:${minutes}</small>
                </div>
                <p>${event['description']}</p>
                ${locationHtml}
                </a>
                `
        }

        $(document).ready(function () {
            showDay(0);
        });

        $(document).ajaxComplete(function(){
            $('#loading').hide();
        })

    </script>



@endsection