@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Register</div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('organizer.register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('locationName') ? ' has-error' : '' }}">
                    {{Form::label('locationName', 'Naam locatie', array('class' => 'col-md-4 control-label'))}}
                    <div class="col-md-6">
                        {{Form::input('input','locationName', null, array('class' => 'form-control select'))}}
                        @if ($errors->has('locationName'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('locationName') }}</strong>
                                    </span>
                        @endif
                    </div>

                </div>
                <div class="form-group{{ $errors->has('locationAddress') ? ' has-error' : '' }}">
                    {{Form::label('address', 'Adres locatie', array('class' => 'col-md-4 control-label'))}}
                    <div class="col-md-6">
                        {{Form::input('input','locationAddress', null, array('class' => 'form-control'))}}
                        @if ($errors->has('locationAddress'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('locationAddress') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>


                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Register
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection



