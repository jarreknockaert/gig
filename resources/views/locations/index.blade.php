@extends('layouts.app')

@section('content')
    <table class="table">
        <tr>
            <th>Naam</th>
            <th>Adres</th>
            <th></th>
            <th></th>
        </tr>
        @foreach($locations as $location)
            <tr>
                <td>{{$location->name}}</td>
                <td>{{$location->address}}</td>
                <td><a href="{{route('locations.edit', ['id' => $location->id])}}">Wijzig</a></td>
                <td><button type="button" class="btn btn-link" onclick="document.getElementById('remove-location').submit()">Verwijder</button>
                    {{ Form::open(array('route' => array('locations.destroy', $location), 'method' => 'DELETE', 'id' => 'remove-location')) }}</td>
            </tr>
        @endforeach
    </table>

@endsection